{config, pkgs, ...}:

{
	# enable routing
	boot.kernel.sysctl = {
		"net.ipv4.ip_forward" = 1;
	};
	networking = {
	
		hostName = "wgrouter";
		domain = "vpndmz.local";

		# disable dhcp AND forbid dhcp requests on lan NIC
		dhcpcd.denyInterfaces = [ "ens18" ];
		dhcpcd.enable = false;

		# firewall stuff


		firewall = {
			enable = true;
			extraCommands = ''
				# default drop
				iptables -P FORWARD DROP

				# default accept all established
				iptables -A FORWARD -m state --state ESTABLISHED,RELATED -m comment --comment "## Accept all traffic from ongoing connections" -j ACCEPT

				# VPN Routing - wg0
				iptables -A FORWARD -m state --state NEW -i wg0 -o wg0 -m comment --comment "## Accept all traffic from wireguard to wireguard"  -j ACCEPT 
				iptables -A FORWARD -m state --state NEW -i wg0 -o ens18 -s 10.253.0.0/24 -d 10.253.42.0/24 -m comment --comment "## Allow ping from vpn endpoints into dmz net" -j ACCEPT
				iptables -A FORWARD -m state --state NEW -i wg0 -o ens18 -s 10.253.66.0/24 -d 10.253.42.0/24 -m comment --comment "## Accept from exec to hase" -j ACCEPT
				iptables -A FORWARD -m state --state NEW -i wg0 -o ens18 -s 10.253.69.0/24 -d 10.253.42.0/24 -m comment --comment "## Accept from mikey to hase" -j ACCEPT
				iptables -A FORWARD -m state --state NEW -i ens18 -o wg0 -s 10.253.42.0/24 -d 10.253.0.0/16 -m comment --comment "## Accept from hase to every vpn" -j ACCEPT
				
				# VPN Routing - wg1
				iptables -A FORWARD -m state --state NEW -i wg1 -o ens18 -s 192.168.4.40 -d 10.0.2.0/27 -m comment --comment "## Allow ping from tablet into network" -j ACCEPT
				iptables -A FORWARD -m state --state NEW -i wg1 -o ens18 -s 192.168.4.40 -d 10.10.10.0/24 -m comment --comment "## Allow ping from tablet into network" -j ACCEPT
				iptables -A FORWARD -m state --state NEW -i wg1 -o ens18 -s 192.168.4.40 -d 192.168.42.0/27 -m comment --comment "## Allow ping from tablet into network" -j ACCEPT
				iptables -A FORWARD -m state --state NEW -i wg1 -o ens18 -s 192.168.4.40 -d 10.253.0.0/16 -m comment --comment "## Allow ping from tablet into network" -j ACCEPT
				iptables -A FORWARD -m state --state NEW -i wg1 -o wg1 -m comment --comment "## Accept all traffic from wireguard to wireguard" -j ACCEPT 
				
				# routing from internal WG to VPN
				iptables -A FORWARD -m state --state NEW -i ens18 -o wg0 -s 10.10.10.0/24 -d 10.253.0.0/16 -m comment --comment "## Accept from hase to everyone" -j ACCEPT
				iptables -A FORWARD -m state --state NEW -i ens18 -o wg1 -m comment --comment "## Accept from hase to wg1-everyone" -j ACCEPT

				# routing from dmz to internal
				iptables -A FORWARD -m state --state NEW -i ens18 -o ens18 -s 10.253.42.0/24 -m comment --comment "## Accept from dmz VMs to internal" -j ACCEPT

			'';
			extraStopCommands = ''
				#iptables -P FORWARD ACCEPT
				iptables -F FORWARD
			'';

			allowedTCPPorts = [ 
				22 # ssh
				179 # bgp
			];
			allowedUDPPorts = [
				51820 # wireguard
			];


		};
		usePredictableInterfaceNames = true;
		#vlans = {
      		#	"ens3.4056" = { id = 4056; interface = "ens3"; };
		#};
		interfaces.ens18 = {
			ipv4.addresses = [
				{
					address = "10.253.42.2";
					prefixLength = 24;
				}
			];
		};
		#interfaces."ens3.4056" = {
		#	ipv4.addresses = [
		#		{
		#			address = "192.168.3.5";
		#			prefixLength = 24;
		#		}
		#	];
		#};

		defaultGateway = {
			address = "10.253.42.1";
			interface = "ens18";
		};

		nameservers = [
			"10.0.2.7"
		];
	};
}

