{ config, pkgs, ... }:

{
	# enable SSH Agent Login on ssh daemon
	security.pam.enableSSHAgentAuth = true;

	# activate sudo with SSH Key
	security.sudo.extraConfig = ''
		Defaults env_keep+= "SSH_AUTH_SOCK"
	'';

}
