{ config, pkgs, ... }:

{

	packages.unfree = [ ];
	#nixpkgs.config.allowUnfree = true;

	environment.systemPackages = with pkgs; [
		wget htop vim git sl speedtest-cli ncdu iftop iotop mtr tcpdump dnsutils qrencode telnet wireguard wireguard-tools
	];

}		
